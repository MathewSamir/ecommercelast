import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appImageSrc]'
})
export class ImageSrcDirective {
  @Input() userImage:string;
  constructor(private eRef:ElementRef) { }


  @HostListener('error')
 loadFallbackOnError()
 { 
   const element:HTMLImageElement = <HTMLImageElement> this.eRef.nativeElement;
   // element.src = this.userImage||'https://drive.google.com/open?id=1ldsThrDlqk1nV1xUkm55IkjlBe-X1HGp'
   element.src = this.userImage//https://www.flaticon.com/premium-icon/icons/svg/2889/2889016.svg'
 }




}
