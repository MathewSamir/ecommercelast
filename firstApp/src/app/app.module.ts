import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {ToastrModule} from 'ngx-toastr'
import {ConfirmationPopoverModule} from 'angular-confirmation-popover'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductItemComponent } from './components/products/product-item/product-item.component';
import { PormotionComponent } from './components/pormotion/pormotion.component';
import { AboutComponent } from './components/about/about.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { CartComponent } from './components/cart/cart.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';
import { ProductDataService } from './Services/product.service';
import { LoginService } from './Services/login/login.service';
import { RegisterService } from './Services/register/register.service';
import { AuthService } from './Services/auth/auth.service';
import { ProfileComponent } from './components/profile/profile.component';
import { LogoutComponent } from './components/logout/logout/logout.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { CartItemComponent } from './components/cart/cart-item/cart-item/cart-item.component';
import { OrdersAdminComponent } from './components/orders-admin/orders-admin.component';
import { AdminProductsComponent } from './components/admin-products/admin-products.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { from } from 'rxjs';
import { CreateProductComponent } from './components/create-product/create-product.component';

import { FooterComponent } from './components/footer/footer.component';
import { FashionComponent } from './components/fashion/fashion.component';
import { FeaturesComponent } from './components/features/features.component';
import { DetailsComponent } from './components/details/details.component';
import { OriginalLoginComponent } from './components/originalLogin/originalLogin.component';
import { NotAuthorizedComponent } from './components/not-authorized/not-authorized.component';
import { AuthGuardService } from './Services/auth-guard.service';
import { ImageSrcDirective } from './image-src.directive';
import { UserImageDirectiveDirective } from './directives/user-image-directive.directive';
import { ProductImageDirectiveDirective } from './directives/product-image-directive.directive';
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'profile', component: ProfileComponent,canActivate: [AuthGuardService], data: { roles: ["Admin","Normal User"]}},
  { path: 'editProfile', component: EditProfileComponent,canActivate: [AuthGuardService], data: { roles: ["Admin","Normal User"]}},
  { path: 'adminOrders', component: OrdersAdminComponent,canActivate: [AuthGuardService], data: { roles: ["Admin"]}},
  { path: 'adminCreateProducts', component: CreateProductComponent },
  { path: 'notAuthorized', component: NotAuthorizedComponent },
  { path: 'originalLogin', component:  OriginalLoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'cart', component: CartComponent ,canActivate: [AuthGuardService], data: { roles:["Normal User"]}},
  { path: 'products', component: ProductsComponent,canActivate: [AuthGuardService], data: { roles: ["Admin","Normal User"]} },
  { path: 'adminProducts', component: AdminProductsComponent,canActivate: [AuthGuardService], data: { roles: ["Admin"]} },
  { path: 'adminProducts/:id', component: ProductDetailsComponent,canActivate: [AuthGuardService], data: { roles: ["Admin"]} } ,
  { path:'details/:id',component:DetailsComponent,canActivate: [AuthGuardService], data: { roles: ["Admin","Normal User"]}},
  { path: '**', component: ErrorComponent },
  { path: '', component: AppComponent }

];

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductItemComponent,
    PormotionComponent,
    AboutComponent,
    LoginComponent,
    RegisterComponent,
    CartComponent,
    HomeComponent,
    ErrorComponent,
    ProfileComponent,
    LogoutComponent,
    CartItemComponent,
    EditProfileComponent,
    AdminProductsComponent,
    ProductDetailsComponent,
    OrdersAdminComponent,
    CreateProductComponent,
    FooterComponent,
    FashionComponent,
    FeaturesComponent,
    DetailsComponent,
    OriginalLoginComponent,
    NotAuthorizedComponent,
    ImageSrcDirective,
    UserImageDirectiveDirective,
    ProductImageDirectiveDirective
  ],

  imports: [
    NgbModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      progressBar:true,
      timeOut:1500,
      progressAnimation:"increasing",
      preventDuplicates:true
    }),
ConfirmationPopoverModule.forRoot({
 confirmButtonType:'danger'
}),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  
  ],
  providers: [ProductDataService, LoginService, RegisterService, AuthService],
  bootstrap: [AppComponent],

})
export class AppModule {}
