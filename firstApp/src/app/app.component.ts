import {
  Component,
  ComponentRef,
  ViewChild,
  AfterViewInit,
  OnInit,
  Input,
  ElementRef,
  HostListener,
} from '@angular/core';
import { RegisterComponent } from './components/register/register.component';
import { AuthService } from './Services/auth/auth.service';
import { MessangerService } from './Services/messanger.service';
import { NgZone } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { Product } from 'src/app/Model/product.Model';
import { CartService } from 'src/app/Services/cart.service';
import { LoginService } from './Services/login/login.service';
import { Router } from '@angular/router';
import { JsonpInterceptor } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'firstApp';
  id;
  currentUser; 
  isAdmin = false;


  name;
  role;
  hasToken = false;
  userID;
  isNormalUser = false;
  IsAdmin = false;
  IsName = false;
  @Input() numberofproducts = 0;
  @Input() userImage;
  constructor( private toastr:ToastrService,private loginService:LoginService,private msg: MessangerService,private router:Router,private authSerivce: AuthService,private cartService:CartService ,private zone :NgZone,public translate :TranslateService,private userProfile : LoginService) {
    console.log(localStorage.getItem('token'));
    translate.addLangs(['en','ar']);
    translate.setDefaultLang('en');
    const browserLang=translate.getBrowserLang();
    translate.use(browserLang.match(/en | ar/) ? browserLang:'en'); 
  }
  
  @HostListener('error')

  
  
  
  ngOnInit(): void {



    this.logout();
    this.userID = localStorage.getItem('userID');
    if(this.userID)
    {
      this.updateCartQuantity(this.userID);
      this.updateProfileInfo(this.userID);
    }
  

    this.handleSubscription();
    
  }



  validateRole(userObject) {
    if (userObject.role === 'Normal User') {
      this.isNormalUser = true;
    } else if (userObject.role === 'Admin') {
      this.IsAdmin = true;
    }
  }

   updateCartQuantity(speceficUserID1)
   {
    this.cartService.getCartQuantity(speceficUserID1).subscribe((res:any)=>{
          this.numberofproducts = res.quantity;
       })
   }


   updateProfileInfo(speceficUserID2)
   {

    
    
    this.loginService.getUser(speceficUserID2).subscribe(res=>{
    //  this.toastr.success("successfully logged in","Welcome");
      this.currentUser = res;
         this.name = this.currentUser.name;
         this.IsName = true;
     
     if(this.currentUser.role ==="Admin")
        this.IsAdmin= true;
      else
      {
        this.isNormalUser = true
        this.cartService.getCartQuantity(speceficUserID2).subscribe((res:any)=>{
          this.numberofproducts = res.quantity;
       })
      
      }  
      this.userProfile.updateImagePathForUserProfile(this.currentUser);
        this.userImage = this.currentUser.userImage;
        console.log(this.userImage);

     
    }) 
   }


  handleSubscription() {
     
    this.msg.getMsg().subscribe((product: Product) => {
     
      this.userID = localStorage.getItem('userID');
      this.updateCartQuantity(this.userID);})
      this.msg.getUserName().subscribe((user:any)=>{
      this.userID = localStorage.getItem('userID');
      this.updateProfileInfo(this.userID)

    });
  }



  logout()
  {
    this.msg.logoutEvent().subscribe(
      () => {
        console.log('here our subsc');
        this.name = undefined;
        this.IsName = false;
        this.IsAdmin = false;
        this.isNormalUser = false;
        this.toastr.success("successfully logged out","Bye");
        this.router.navigate(['/home']);
      },
      (err) => {
        console.log('didnot subsc');
      }
    );
  }
  
  isAuthenticated(): Boolean {
    let token = localStorage.getItem('token');
    if (token) return true;
    else return false;
  }
}
