import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { productsUrl } from 'src/app/config/api';

@Injectable({
  providedIn: 'root'
})
export class ProductDeatilsService {

  constructor(private myProduct:HttpClient) { }

  getProdctInfo(id){
    return this.myProduct.get(`${productsUrl}/${id}`);
  }
}
