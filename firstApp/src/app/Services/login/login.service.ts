import { Injectable, SecurityContext } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser'
import {ImageUrl} from 'src/app/config/api'
import {authUrl} from 'src/app/config/api';
import {userUrl} from 'src/app/config/api'


@Injectable({
  providedIn: 'root',
})
export class LoginService {
  // baseUrl: string = 'http://localhost:3000/api/auth';

  constructor(private myClient: HttpClient,private sanitization:DomSanitizer) {}

  addUser(userObj): any {
    return this.myClient.post(authUrl, userObj);
  }

  updateImagePathForUserProfile(user)
  {

     var actualPath = user.userImage.slice(8);    
     var currentPath= ImageUrl + actualPath;   
      user.userImage = this.sanitization.sanitize(SecurityContext.RESOURCE_URL, this.sanitization.bypassSecurityTrustResourceUrl(currentPath));

  }

 getUser(userID)
 {
   return this.myClient.get(userUrl+"/"+userID);

 }
   


}
