import { Injectable, TemplateRef } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessangerService {

  subject1 = new Subject();
  subject2 = new Subject();
  subject3 = new Subject();
  subject4 = new Subject();
  subject5 = new Subject();




  constructor() { }

   sendMsg(product)
   {
     this.subject1.next(product); // trigger event
   }
   getMsg()
   {
       return this.subject1.asObservable();
   }

   sendUserName(userName)
   {
     this.subject2.next(userName);
   }
   getUserName(){
     return this.subject2.asObservable();
   }
   
   fireLogout() {
    this.subject3.next('');
  }

  logoutEvent() {
    return this.subject3.asObservable();
  }


  notifyDeleteProduct(product) {
    this.subject4.next('');
  }

  loadProductsAfterDelete() {
    return this.subject4.asObservable();
  }


  sendModalContent(content) {
    this.subject5.next(content);
  }
  getModalContent() {
    return this.subject5.asObservable();
  }





}
