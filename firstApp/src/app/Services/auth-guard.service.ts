import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './login/login.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  currentUser;
  userRole;
  isAuthorized = false;
  constructor(private router: Router,private loginService:LoginService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    if(this.isAuthenticated())
    {
      this.authorizedUser(route);  
      return this.isAuthorized;
    
    }
    
    this.router.navigate(['/home']);
    return false;
    
}

isAuthenticated(): Boolean {
  let token = localStorage.getItem('token');
  if (token) return true;
  else return false;
}

 authorizedUser(route:ActivatedRouteSnapshot)
{
  let role = localStorage.getItem("Role");
  route.data.roles.forEach(element => {
    if(element===role)
       {
         this.isAuthorized = true
         return true;
       }
  });

  if(!this.isAuthorized) 
  { 
     this.router.navigate(['/notAuthorized']);
     return false;
  }

}


}
