import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {userUrl} from 'src/app/config/api'

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  // baseUrl = 'http://localhost:3000/api/users';

  constructor(private registerClient: HttpClient) {}

  createUser(userObj): any {
    let formData = new FormData();
  

    formData.append('name', userObj.name);
    formData.append('email', userObj.email);
    formData.append('password', userObj.password);
   
    formData.append('userImage', userObj.file ,userObj.file.name )
    // console.log(userObj)
    console.log(formData); 
    return this.registerClient.post(userUrl, formData);
    // return this.registerClient.post(this.baseUrl, userObj);
  }

  editUser(userObj,id): any {
    let formData = new FormData();
    formData.append('name', userObj.name);
    formData.append('email', userObj.email);
    console.log("old password is",userObj.password);
    formData.append('password', userObj.password);
    formData.append('userImage', userObj.file ,userObj.file.name )
    console.log(formData); 
    return this.registerClient.put(userUrl+"/"+id, formData);
  }











}
