import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JsonPipe } from '@angular/common';
import {authUrl} from 'src/app/config/api';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(private authClient: HttpClient) {}

  authorizeUser(token): any {
    return this.authClient.get(authUrl+'/me', { headers: token });
  }
 
 


}
