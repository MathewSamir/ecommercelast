import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { orderUrl } from '../config/api';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private myClient: HttpClient) { }
  
  getOrders() {
    return this.myClient.get(orderUrl);
  }
  editStatus(orderId,newStatus){
    return this.myClient.patch(`${orderUrl}/${orderId}`,newStatus);
  }
}
