import { Component, OnInit ,ViewChild} from '@angular/core';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-pormotion',
  templateUrl: './pormotion.component.html',
  styleUrls: ['./pormotion.component.css']
})
export class PormotionComponent implements OnInit {
  //images = [62, 83, 466, 965, 982, 1043, 738].map((n) => `https://picsum.photos/id/${n}/900/500`);
  images = ["https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/98097129_3264475116948458_6646278220578553856_n.jpg?_nc_cat=102&_nc_sid=8024bb&_nc_ohc=I0oZ-1_u4tkAX_kDaZJ&_nc_ht=scontent-hbe1-1.xx&oh=6c53e85bc961ab680547296fa3b5cdd1&oe=5EE00A55"
  ,"https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/97403601_3264475163615120_1008684713121939456_n.jpg?_nc_cat=101&_nc_sid=8024bb&_nc_ohc=aDLVY_TYx5MAX9PjI5u&_nc_ht=scontent-hbe1-1.xx&oh=0eabe30f7c329b4d5e76b0cbdfa132c0&oe=5EE0988B",
  "https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/97415837_3264475153615121_813486270654185472_n.jpg?_nc_cat=111&_nc_sid=8024bb&_nc_ohc=yZi1YhLrQOAAX8L518m&_nc_ht=scontent-hbe1-1.xx&oh=290b3aa12f77cf37eb9c722f25723eb2&oe=5EE2AF84",
"https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/97049415_3264475216948448_7004437596386361344_n.jpg?_nc_cat=108&_nc_sid=8024bb&_nc_ohc=TX1b85CiLJMAX9E-chB&_nc_ht=scontent-hbe1-1.xx&oh=16b12ea0375edcb5ec838c3788ee3bc5&oe=5EE0CDE6",
"https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-0/p526x395/96529482_3264475493615087_3673089089902477312_o.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_ohc=hezPnvaG6CwAX83Z8yh&_nc_ht=scontent-hbe1-1.xx&_nc_tp=6&oh=0b8feeb350d7db8024e6bf49ebf89d99&oe=5EE1D158",
"https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/97107189_3264475530281750_3730402499299901440_n.jpg?_nc_cat=104&_nc_sid=8024bb&_nc_ohc=WGaL0-g7OsEAX94EAzx&_nc_ht=scontent-hbe1-1.xx&oh=cc0626b2c436794d2d5f26998db84688&oe=5EE1C87F",
"https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/96584605_3264475563615080_2433773205740060672_n.jpg?_nc_cat=108&_nc_sid=8024bb&_nc_ohc=EyROEZ4-OX8AX9_16d0&_nc_ht=scontent-hbe1-1.xx&oh=7a0051796218ed2bdfef3a413c5f2c5f&oe=5EE2369B",
]
  
  
 
  paused = false;
  unpauseOnArrow = false;
  pauseOnIndicator = false;
  pauseOnHover = true;

  @ViewChild('carousel', {static : true}) carousel: NgbCarousel;

  togglePaused() {
    if (this.paused) {
      this.carousel.cycle();
    } else {
      this.carousel.pause();
    }
    this.paused = !this.paused;
  }

  onSlide(slideEvent: NgbSlideEvent) {
    if (this.unpauseOnArrow && slideEvent.paused &&
      (slideEvent.source === NgbSlideEventSource.ARROW_LEFT || slideEvent.source === NgbSlideEventSource.ARROW_RIGHT)) {
      this.togglePaused();
    }
    if (this.pauseOnIndicator && !slideEvent.paused && slideEvent.source === NgbSlideEventSource.INDICATOR) {
      this.togglePaused();
    }
  }
  constructor() { }

  ngOnInit(): void {
  }

}
