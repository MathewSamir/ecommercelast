import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/Services/orders.service';
import { LoginService } from 'src/app/Services/login/login.service';
import { Router,CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-orders-admin',
  templateUrl: './orders-admin.component.html',
  styleUrls: ['./orders-admin.component.css']
})
export class OrdersAdminComponent implements OnInit  {
  orders;
  newStatus;
  id;
  userRole;
  currentUser;
  constructor(private toastr:ToastrService,private loginService:LoginService,private service:OrdersService,private router:Router) { }
  



  ngOnInit(): void {
    this.getOrders();
  }









  getOrders()
  {
    this.service.getOrders().subscribe((res) => {
      this.orders=res;
        console.log(res)
      }, (err) => { console.log(err) })

      this.service.editStatus(this.id,this.newStatus).subscribe((res) => {
        this.orders.push(res)
          console.log(res)
        }, (err) => { console.log(err) })

  }

  selectStatus(id:string,stat:string){
    //this.orders.orderStatus=stat;
  this.service.editStatus(id,{orderStatus:stat}).subscribe((res) => {
    
    this.toastr.success("Status Of Order Updated","Success");
    this.orders=res;
    this.getOrders();
  
    }, (err) => { console.log(err) })
  }



  isAuthenticated(): Boolean {
    let token = localStorage.getItem('token');
    if (token) return true;
    else return false;
  }

  authorizedUser()
  {
     let currentUserID = localStorage.getItem("userID");
     this.loginService.getUser(currentUserID).subscribe(res=>{
      this.currentUser = res;
      
       console.log("User Now is",this.currentUser);

       if(this.currentUser.role ==="Admin")
       {
          this.getOrders();
       }    
        //this.userRole= "Admin";
        else
           this.router.navigate(['/notAuthorized']);
      })
  }

}


