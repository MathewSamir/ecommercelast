import {
  Component,
  OnInit,
  ViewEncapsulation,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  AfterViewInit,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from 'src/app/Services/login/login.service';
import { AuthService } from 'src/app/Services/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessangerService } from 'src/app/Services/messanger.service';

@Component({
  selector: 'app-originalLogin',
  templateUrl: './originalLogin.component.html',
  encapsulation: ViewEncapsulation.Emulated,
  //inputs:['content'],
  styleUrls: ['./originalLogin.component.css'],
})
export class OriginalLoginComponent implements OnInit {
  img = `assets/images/newsletter-popup.jpg`;
  closeResult: string;
  email;
  isValidEmail: boolean;
  isWrongEmailOrPass: boolean;

  isValidPass: boolean;
  password;
  userName;

  @Output() myEvent = new EventEmitter();

  constructor(
    private modalService: NgbModal,
    private loginService: LoginService,
    private authSerivce: AuthService,
    private router: Router,
    private msg: MessangerService
  ) {}

  // ngAfterViewInit(): void {
  //   // throw new Error("Method not implemented.");
  //   this.openLg(this.content);

  // }
  // ngAfterViewChecked(): void {
  //   this.openLg(this.content);
  // }

  ngOnInit(): void {
  
    this.msg.getModalContent().subscribe((content)=>{
       this.openLg(content);
    })
    

  }
 // @ViewChild('content', { static: true }) content: ElementRef ;
  //myContent = this.content;

  /* #region  ui funs */
  openBackDropCustomClass(content) {
    this.modalService.open(content, { backdropClass: 'light-blue-backdrop' });
  }

  openWindowCustomClass(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }

  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }

  openLg(content) {
    console.log(content);
     console.log(typeof(content));

    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeResult = `closed with ${result}`;
    });
  }

  openXl(content) {
    this.modalService.open(content, { size: 'xl' });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  openScrollableContent(longContent) {
    this.modalService.open(longContent, { scrollable: true });
  }

  /* #endregion */

  login(): any {
    this.isValidEmail = false;
    this.isWrongEmailOrPass = false;
    this.isValidPass = false;
    let userData = {
      email: this.email,
      password: this.password,
    };
    this.loginService.addUser(userData).subscribe(
      (res) => {
        localStorage.setItem('token', res.token);
      
       
        this.modalService.dismissAll();
        console.log(this.userName);
        let token = localStorage.getItem('token');
        console.log(typeof token);
        this.authSerivce.authorizeUser({ token: token }).subscribe(
          (s) => {
            this.userName = s.name;
      
            localStorage.setItem('userID',s._id);
            localStorage.setItem('Role',s.role);

             this.msg.sendUserName(s);
             this.router.navigate(['products']);
          },
          (err) => {
            console.log(err);
          }
        );
      },
      (err) => {
        let emailError = 'email" is required';
        console.log(err);
        let passError = 'password" is required';
        // let passError2 = 'password" is not allowed to be empty';
        if (err.error === '"' + emailError) {
          console.log('match', err);
          this.isValidEmail = true;
        } else if (err.error === '"' + passError) {
          this.isValidPass = true;
        } else if (err.error === 'Invalid email or password') {
          this.isWrongEmailOrPass = true;
        }
      }
    );
  }

  isAuthenticated(): Boolean {
    let token = localStorage.getItem('token');
    if (token) return true;
    else return false;
  }
  

  



}
