import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-admin-product-item',
  templateUrl: './admin-product-item.component.html',
  styleUrls: ['./admin-product-item.component.css']
})
export class AdminProductItemComponent implements OnInit {

  @Input() productItem;
  constructor() { }

  ngOnInit(): void {
  }

}
