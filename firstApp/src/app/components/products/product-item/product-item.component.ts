import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/Model/product.Model';
import { CartService } from 'src/app/Services/cart.service';
import {MessangerService} from 'src/app/Services/messanger.service';
import { AuthService } from 'src/app/Services/auth/auth.service';
import { ProductDeatilsService } from 'src/app/Services/product-deatils.service';
import { Router } from '@angular/router';
import { ProductDataService } from 'src/app/Services/product.service';
import { stringify } from 'querystring';
import { LoginService } from 'src/app/Services/login/login.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  
  public popoverTitle:string="Confirmation";
  public popoverMessage:string="Sure Delete?";
  public confirmClicked:boolean = false;
  public cancelClicked:boolean = false;


 



  @Input() productItem:Product;
  userid;
id;
currentUser;
isAdmin = false;
  constructor(private toastr:ToastrService,private loginService:LoginService,private msg: MessangerService, private router: Router,
    private cartService: CartService,private authService:AuthService,private prodDetails:ProductDeatilsService,private productService:ProductDataService) { }

  ngOnInit(): void {
    let token = localStorage.getItem('token');
    this.userid = localStorage.getItem('userID');
   
    this.loginService.getUser(this.userid).subscribe(res=>{
      this.currentUser = res;
      
     
     if(this.currentUser.role ==="Admin")
        this.isAdmin= true;

  })
  }
  
   handleAddToCart() {  
     (
     
       
        this.cartService.addProductToCart(this.productItem._id, this.userid)).subscribe(() => {
          this.toastr.success("Product Add to Cart","Success")
           this.msg.sendMsg(this.productItem)
     })
   
  }
  showDetails(){
    this.prodDetails.getProdctInfo(this.productItem._id).subscribe((res)=>{console.log(res)},(err)=>{console.log(err)})
  }
  routeDetails(id){
    this.id=this.productItem._id;
    this.router.navigate(['/details',this.id]);
  }

  deleteProduct()
  {
    this.id=this.productItem._id;
    this.productService.deleteProduct(this.productItem._id).subscribe((res)=>{
      this.toastr.success("Product Deleted","Success")
      this.msg.notifyDeleteProduct(res);
       
    
    },
      
      (err)=>{console.log(err)})
  }

  editProduct()
  {
    this.id=this.productItem._id;
    this.router.navigate(['/adminProducts',this.id]);

  }


}
