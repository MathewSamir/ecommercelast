import { Component, OnInit, SecurityContext, ElementRef, TemplateRef, ViewChild } from '@angular/core';
import {Product} from 'src/app/Model/product.Model'
import { ProductDataService } from 'src/app/Services/product.service';
import { Router } from '@angular/router';
import { MessangerService } from 'src/app/Services/messanger.service';
import { LoginService } from 'src/app/Services/login/login.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/Services/auth/auth.service';





@Component({
  selector: 'app-products',

  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  
  term;
  id;
  currentUser; 
  isAdmin = false;
  products$:Product[] ;
 
 
   constructor(private modalService: NgbModal,private msg:MessangerService,private productService:ProductDataService, private router: Router,private messanger:MessangerService,private loginService:LoginService,
    private authSerivce: AuthService) {}
  // loginPage;
  
  ngOnInit(){
  
      this.loadProducts();
      this.handleSubscriptions();
      this.id = localStorage.getItem('userID');
      this.loginService.getUser(this.id).subscribe(res=>{
        this.currentUser = res;
       if(this.currentUser.role ==="Admin")
          this.isAdmin = true;
 
      }) 
  }
   
  loadProducts() {
    this.productService.getProducts().subscribe((products) => {   
      this.products$ = products;
       this.productService.updateImagesPathForProduct(this.products$); 
    })
  }
  routeCreate(){
    this.router.navigate(['adminCreateProducts']);
  }

  
  handleSubscriptions()
  {
    {
      this.messanger.loadProductsAfterDelete().subscribe(() => {
        this.loadProducts();
      
      })
    }
  }
  
  isAuthenticated(): Boolean {
    let token = localStorage.getItem('token');
    if (token) return true;
    else return false;
  }

}
