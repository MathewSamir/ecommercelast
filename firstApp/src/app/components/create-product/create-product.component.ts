import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Product } from 'src/app/Model/product.Model';
import { ProductDataService } from 'src/app/Services/product.service';
import { ITS_JUST_ANGULAR } from '@angular/core/src/r3_symbols';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';


// import { LoginService } from 'src/app/services/login/login.service';
// import { Router } from '@angular/router';


@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {
  selectedFile: File;
  registerForm: FormGroup;
  submitted = false;
  product:any = null
  currentUser;
 

  constructor(private toastr:ToastrService,private modalService: NgbModal
    ,private formBuilder:FormBuilder,private productService:ProductDataService) { }

  ngOnInit(): void {
   
     this.registerForm = this.formBuilder.group({
      title: ['',[Validators.required,Validators.minLength(7),Validators.max(25)]],
         description: ['', Validators.required],    
         price: ['', Validators.required],
         imagePath: ['', [Validators.required]],
  });
  }

  get f() { return this.registerForm.controls; }

  openSm(content) {
    this.modalService.open(content, { size: 'lg' });
  }


  onSubmit(content) {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
          else
        {
          this.product = {
            title :JSON.stringify(this.registerForm.value.title),
            description:JSON.stringify(this.registerForm.value.description),
            price:JSON.stringify(this.registerForm.value.price),
            file:this.selectedFile, 
          }

           this.productService.addProduct(this.product).subscribe((res)=>{
             this.onReset();
              this.toastr.success("Product Created Successfuly","Success")
             // this.openSm(content);
            });
         
        }
      // display form values on success
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  onFileSelected(evetObj) {
    this.selectedFile = evetObj.target.files[0];
    //console.log(this.selectedFile);
  }


  onReset() {
    
    
    if(this.submitted===true&&!this.registerForm.invalid)
     // this.openSm();
      
      this.registerForm.reset();

    this.submitted = false;

}



}

