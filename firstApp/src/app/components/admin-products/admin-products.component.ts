import { Component, OnInit } from '@angular/core';
import { ProductDataService } from 'src/app/Services/product.service';

@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnInit {

  products ; 

  constructor(private productService : ProductDataService) { }

  ngOnInit(): void {

    this.productService.getProducts().subscribe((products) =>{
      this.products = products;
    })
  }

}
