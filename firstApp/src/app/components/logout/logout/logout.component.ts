import { Component, OnInit } from '@angular/core';
  import { MessangerService } from 'src/app/Services/messanger.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
isLoggedIn : boolean = false; 
  constructor(private messangerService:MessangerService) { }

  ngOnInit(): void {
    let token =  localStorage.getItem('token');
    if(token)
    {
      this.isLoggedIn =true;
    }
  }
  
  logout(){
    this.messangerService.fireLogout();
    console.log("logout hereeeeeeeeeeeeeeeeeee")
    localStorage.removeItem('token');
    localStorage.clear();
    

  }
}
