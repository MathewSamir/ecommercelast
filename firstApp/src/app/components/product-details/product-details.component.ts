import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductDataService } from 'src/app/Services/product.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  id;
  subscriber;
  product;
  selectedFile;
  submitted = false;
  title;
  @Input() productImage;

  constructor(
    private myActivatedRoute: ActivatedRoute,
    private productService: ProductDataService,
    private router : Router,
    private toastr:ToastrService,
    private formBuilder:FormBuilder
  ) {
    this.id = this.myActivatedRoute.snapshot.params['id'];
    console.log(this.id);
  }

  public editProductForm =
  this.formBuilder.group({
     title: ['',[Validators.required,Validators.minLength(7),Validators.max(25)]],
     description: ['', Validators.required],    
     price: ['', Validators.required]})
  
  
  // new FormGroup({
  //   title: new FormControl('',[Validators.required,Validators.minLength(7),Validators.maxLength(255)]),
  //   description : new FormControl('',[Validators.minLength(5),Validators.maxLength(1024)]),
  //   price : new FormControl('',Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
  // });

  get f() { return this.editProductForm.controls; }

  get titleStatus(){return this.editProductForm.controls.title.valid}
  get descriptionStatus(){return this.editProductForm.controls.description.valid}
  get priceStatus(){return this.editProductForm.controls.price.valid}


  ngOnDestroy(): void {
    this.subscriber.unsubscribe();
  }

  ngOnInit(): void {
    this.getProduct();
  }

  getProduct() {
    this.subscriber = this.productService.getProductById(this.id).subscribe(
      (product) => {
        // if (product)
         this.product = product;
         this.productService.updateImagePathForProduct(this.product);
          this.productImage = this.product.imagePath;
          this.createFile(this.productImage);
         
      },
      (err) => {
        console.log(err);
      }
    );
  }
  onFileSelected(evetObj) {
    //console.log(data.target.files[0]);
    this.selectedFile = evetObj.target.files[0];
  }


  saveChanges() {
    this.submitted = true;
    console.log(this.editProductForm.controls);

    if(this.editProductForm.valid){
       if(this.editProductForm.controls.title.value!==""){
         this.product.title = this.editProductForm.controls.title.value
       
       }
      }
    let updatedProduct = {
      title: this.product.title,
      description: this.product.description,
      price: this.product.price,
      file: this.selectedFile,
    };
 
   
  
    this.productService.updateProduct(this.product._id,updatedProduct).subscribe(
      (product) => {
        
        this.toastr.success("Product Updated","Success")
       // alert('your product updated successfully');
        this.router.navigate(['products'])
      },
      (err) => {
        console.log(err.message.details);
      }
    );

  }



async  createFile(imageURL){
  let response = await fetch(imageURL);
  let data = await response.blob();
  let metadata = {
    type: 'image/*'
  };
  this.selectedFile = new File([data], imageURL, metadata);
  // ... do something with the file or return it
}


}