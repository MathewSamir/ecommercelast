import { Component, OnInit, Output, OnDestroy } from '@angular/core';
//import { RegisterService } from 'src/app/services/register/register.service';
import { RegisterService } from 'src/app/Services/register/register.service'; //  services/register/register.service';

import {
  FormGroup,
  FormControl,
  Validators,
  ReactiveFormsModule,
  FormsModule,
  FormBuilder,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { renderFlagCheckIfStmt } from '@angular/compiler/src/render3/view/template';
import { EventEmitter } from 'events';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/Services/auth/auth.service';
import { LoginService } from 'src/app/Services/login/login.service';
import { MessangerService } from 'src/app/Services/messanger.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  img = `https://www.sony.com.my/image/a7a7277a3ac2458d64b30546a9ff3392?fmt=png-alpha&wid=600`;
  //img = `assets/images/newsletter-popup.jpg`;
  flag: boolean;
  notValidFlag: boolean;
  notValidPassFlag: boolean;
  userName: string ;
  email: string;
  password: string;
  confirmPassword: string;
  selectedFile: File;
  selection;
  myFrom:FormGroup;

  submitted = false;



  @Output() myEvent = new EventEmitter();

  constructor(
    private reigsterService: RegisterService,
    private router: Router,
    private loginSerivce: LoginService,
    private msg: MessangerService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {

     this.myFrom = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.max(20),
      
      ]],
      gender: ['', [Validators.required]],
      image:[''],
      confirmPassword: ['', Validators.required]
  }, {
      validator:this.ConfirmedValidator('password', 'confirmPassword')
  });
  }
    
  
  onFileSelected(evetObj) {
    this.selectedFile = evetObj.target.files[0];
    console.log(this.selectedFile);
    
  }



  signUp() {
    this.submitted = true;
  
    
     console.log("form validation",this.myFrom.invalid)
     console.log(this.allStatus);

    if (!this.myFrom.invalid)
    {
    console.log(this.myFrom.controls['email'].value);
    let user = {
      name: this.myFrom.controls['name'].value,
      email: this.myFrom.controls['email'].value,
      password: this.myFrom.controls['password'].value,
      file: this.selectedFile,
      gender: this.selection,
    };
  
    this.reigsterService.createUser(user).subscribe(
      (res) => {
        console.log(res);
        let resString = JSON.stringify(res);
        // let resJson = JSON.parse(resString);
        let resJson = JSON.parse(resString);
        // this.myEvent.emit(resJson.name);
        console.log(resJson);
        console.log(resJson._id);
        console.log(resJson.name);
        console.log(resJson.email);
        console.log(resJson.gender);

        localStorage.setItem('userID',res._id);
        localStorage.setItem('Role',res.role);
        this.msg.sendUserName(res);
       // this.msg.sendUserName(resJson);

        ///for authentication

        this.loginSerivce
          .addUser({ email: user.email, password: user.password })
          .subscribe((res) => {
            localStorage.setItem('token', res.token);
            //this.msg.sendToken({token :res.token});
            console.log(res);
          });
        this.router.navigate(['products']);
        //this.loginSerivce.updateImagePathForUserProfile(userObj)

      },
      (err) => {
        console.log('create user erro', err.error);
        let v: String;
        let emailValidation: string;
        let passValidation: string;

        v = 'user is already registered';
        emailValidation = "'email' must be a valid email";
        passValidation = "'password' is required";
        var result = err.error.localeCompare(v);
        console.log(err.error);
        let notValidEmail = err.error.localeCompare(emailValidation);
        let notValidPass = err.error.localeCompare(passValidation);
        if (result === 1) this.flag = true;
        else if (notValidPass === 1) {
          this.notValidPassFlag = true;
          notValidPass = 0;
        } else if (notValidEmail === 1) {
          this.notValidFlag = true;
          console.log(notValidEmail);
          notValidEmail = 0;
        }
      }
    );
    }
    else return;
  }


  
  
  
  
  
  

  // get nameStatus() {
  //   return this.myFrom.controls.name.valid;
  // }
  
  get allStatus()
  {
     return this.myFrom.controls;
  }
  

  ConfirmedValidator(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
            return;
        }
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ confirmedValidator: true });
        } else {
            matchingControl.setErrors(null);
        }
      }
    }



}
