import { Directive, Input, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appUserImageDirective]'
})
export class UserImageDirectiveDirective {
  
  @Input() userImage:string;

  constructor(private eRef:ElementRef) { }

  @HostListener('error')
  loadFallbackOnError()
  { 
    const element:HTMLImageElement = <HTMLImageElement> this.eRef.nativeElement;
    element.src = this.userImage||'https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/97999607_3264475263615110_3948639099098759168_n.jpg?_nc_cat=106&_nc_sid=8024bb&_nc_ohc=BRCn4ReH6S0AX_EOBys&_nc_ht=scontent-hbe1-1.xx&oh=8f7a91cabba86edf7e35ca2623a2eea4&oe=5EE2D961'
  }

}
