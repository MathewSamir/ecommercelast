import { Directive, Input, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appProductImageDirective]'
})
export class ProductImageDirectiveDirective {

  @Input() productImage:string;

  constructor(private eRef:ElementRef) { }

  @HostListener('error')
  loadFallbackOnError()
  { 
    const element:HTMLImageElement = <HTMLImageElement> this.eRef.nativeElement;
   // element.src = this.productImage||'https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/96657922_3264475260281777_8633113843846873088_n.jpg?_nc_cat=102&_nc_sid=8024bb&_nc_ohc=gGFBibYba-gAX9ld8Nr&_nc_ht=scontent-hbe1-1.xx&oh=25693eda34d5cf300c65c00cc7771aea&oe=5EE2D8A4'
     element.src = this.productImage||'https://scontent-hbe1-1.xx.fbcdn.net/v/t1.0-9/96766058_3265784913484145_1132781797365514240_n.jpg?_nc_cat=104&_nc_sid=8024bb&_nc_ohc=RXmJ9H9r148AX_QgINO&_nc_ht=scontent-hbe1-1.xx&oh=21624284ea03faa1554f73a428458db9&oe=5EE0BD47'  
  }
}
