import { environment } from 'src/environments/environment'

//export const baseUrl = environment.production ? 'https://api.huroku.com' : 'http://localhost:3000'
export const baseUrl = 'https://ecommercewithdb.herokuapp.com'
export const productsUrl = baseUrl + '/api/products'
export const userUrl = baseUrl+'/api/users'
export const cartUrl = baseUrl + '/api/carts'
export const authUrl = baseUrl +'/api/auth'
export const orderUrl = baseUrl+'/api/orders'
export const ImageUrl = baseUrl+'/uploads/';

//export const ordersForSpecificUserUrl =baseUrl +`/orders/user/`